
# CENTOS 7 安裝 docker-ce

```
yum update
yum install -y yum-utils   device-mapper-persistent-data   lvm2
yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce-20.10.7-3.el7 -y

systemctl start docker

curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose

chmod +x /usr/local/sbin/docker-compose

echo "vm.max_map_count=262144" >> /etc/sysctl.conf
sysctl -p
```

# 建立 docker swarm (範例)
```
docker swarm init
```

# 建立 es 所需要的 network (範例)
```
docker network create --driver=overlay --attachable elastic
```

# 執行完畢後會出現 join node 語法 (範例)
```
docker swarm join \
--token SWMTKN-1-5cnfbsuctir6tsa9oloi8q3tzu7m7ya259t3gfytgwvbv85n8i-7g3sbr6v4x4uq4nkaliudly47 \
10.0.0.110:2377
```

# 將 node 打上 es 所需要部署位置的 label (範例)
```
docker node update --label-add es.replica=1 danlio-test-v1
docker node update --label-add es.replica=2 danlio-test-v2
docker node update --label-add es.replica=3 danlio-test-v3
```

# 使用 fleutnd 資料夾內的 Dockerfile 包 image 與 編譯 fluent.conf, 並且將 image name tag 編寫至 es/.env

# 目前需要每一台 node 都執行 build 過 fluentd image (範例)
```
docker build -f fluentd/Dockerfile -t danlio:test2 .
```

# 檢查fluentd 是否正常 (範例)
```
curl http://127.0.0.1:24444/api/plugins.flushBuffers
```

# docker nginx access log stdout (範例)
```
docker run --name danlio_nginx --log-driver=fluentd --log-opt fluentd-address=34.170.80.228:24224 nginx
```
---
# 指令說明:
```
# 顯示 node 清單
docker node ls
# 顯示 service 清單
docker service ls

# 發布 服務
docker stack deploy  -c file.yaml  es

# 清除 服務
docker stack rm es

# 檢查 volume
docker volume ls

# volume 詳細資料 (可以看實體路徑)
docker volume inspect es_es01_data
```